package SmallChallenge.ReadCsv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class ReadCsv {

	public static ArrayList<String[]> read(File inp) {
		ArrayList<String[]> readResList = new ArrayList<String[]>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(inp));
			ArrayList<String> tempArrList = new ArrayList<String>();//To store the read sentences
			String data;
			while((data = reader.readLine()) != null) {
				tempArrList.add(data);//Same as reading the whole file at once and split \n
			}
			/* *
			 * Find every element, for ",
			 * if got ", skip next ,
			 * else look for ,
			 * save the thing in front , as an element in another arraylist
			 * DONE
			 * */
//			int arrayLength = 0;
			for(String s : tempArrList) {
				String[] temp = ReadCSVToolbox.lookForCommaOrNLine(s).toArray(
						new String[ReadCSVToolbox.lookForCommaOrNLine(s).toArray().length]);
//				arrayLength += temp.length;
				readResList.add(temp);
			}
			
//			String[] readRes = new String[arrayLength];
//			int j = 0;
//			for(String[] str : readResList) {
//				for(int i = 0; i < str.length; i++) {
//					readRes[j] = str[i];
//					System.out.println(readRes[j].equals("") ? "0" : readRes[j]);
//					j++;
//				}
//			}
			//This part is for the readRes Array
			
			//After all the above code
			/*
			Important variables got
			-readRes >> an array which contains every thing(Qishi no nid this >>I think<<), 
			so in this case
			Comment out first gua(Uncomment if need)
			-readResList >> ArrayList contains interpreted every line as an array
			 */
			reader.close();
		}catch(IOException ioE) {
			System.out.println("IO EXCEPTION Occured");
			ioE.printStackTrace();
		}catch(Exception e) {
			System.out.println("Error Occured");
			e.printStackTrace();
		}
		return readResList;
	}

}

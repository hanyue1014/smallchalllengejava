package SmallChallenge.ReadCsv;

import java.util.ArrayList;

public class ReadCSVToolbox {
		
	public static ArrayList<String> lookForCommaOrNLine(String fromFile) {
		char[] chngeToChar = fromFile.toCharArray();
		ArrayList<String> aryList = new ArrayList<String>();
		StringBuilder strBuild = new StringBuilder("");
		for(int i = 0; i < fromFile.length(); i++) {
			//Loop through string, changing them to characters
			
			//Look for double quotes
			if(chngeToChar[i] == '"') {
				//Start the work of finding the next quote
				//Then add everything before the next quote inside it
				int j = i + 1;
				while(chngeToChar[j] != '"') {
					strBuild.append(chngeToChar[j]);
					j++;
				}//End while means find dao d
				//Finddao le add these things into the arraylist
				//Set i de number
				//Prevent reloop over these numbers
				i = j;
				continue;
			}
			
			if(!(chngeToChar[i] == ',' || chngeToChar[i] == '\n')) {
				//Determine if the character is comma or a new line character
				//If not, put the current into StringBuilder first
				strBuild.append(chngeToChar[i]);
			}else {
				//Check if this comma is inside an opened quote
				
				//If it is, store every thing in front to an arraylist
				//Nvm if ntg has been stored before, an empty string will be added to the array
				aryList.add(strBuild.toString());
				strBuild.setLength(0);//Clear anything inside the string builder
			}
		}
		return aryList;
	}
}

package SmallChallenge.ReadCsv;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Challenge {
	public static void main(String[] args) {
		try {
			File csvConfirmed = new File("src/datasets/ncov_confirmed.csv");
			File csvDeaths = new File("src/datasets/ncov_deaths.csv");
			File csvRecovered = new File("src/datasets/ncov_recovered.csv");
			FileWriter out = new FileWriter(new File("src/output/output.txt"));
			ArrayList<String[]> readResConf = ReadCsv.read(csvConfirmed);
			ArrayList<String[]> readResDeaths = ReadCsv.read(csvDeaths);
			ArrayList<String[]> readResRecov = ReadCsv.read(csvRecovered);
			
			//Works:
			/*
			Loop through the times first
			Loop through all element of each string array(The cities)
			print state/ place, then the number at that time column
			 */
			for(int timeStart = 4; timeStart < readResConf.get(0).length; timeStart++) {
				//Time starts at index 4
				String currentTime = readResConf.get(0)[timeStart];
				out.write("-----" + currentTime + "-----\n");
				for(int i = 1; i < readResConf.size(); i++) {
					//i starts from 1 becuz dont need the first row
					String[] currentRow = readResConf.get(i);
					String currentProvince = currentRow[0];
					String currentCountry = currentRow[1];
					String currentConfirmed = currentRow[timeStart];
					String currentDeaths = readResDeaths.get(i)[timeStart];
					String currentRecovered = readResRecov.get(i)[timeStart];
					boolean skipSpaceAfterProvince = false;
					if(currentProvince.contentEquals("")) {
						skipSpaceAfterProvince = true;
					}
					out.write(currentTime 
							+ " " 
							+ currentProvince
							+ (skipSpaceAfterProvince ? "" : " ")
							+ currentCountry
							+ " "
							+ (currentConfirmed.equals("") ? 0 : currentConfirmed)
							+ " "
							+ (currentDeaths.equals("") ? 0 : currentDeaths)
							+ " "
							+ (currentRecovered.equals("") ? 0 : currentRecovered)
							+ "\n");
				}
			}
			
			out.close();
		}catch(IOException e) {
			System.out.println("Error Occured");
			e.printStackTrace();
		}
	}
}
